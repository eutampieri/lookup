/* TimerWindowController */

#import <Cocoa/Cocoa.h>

@interface TimerWindowController : NSObject
{
    IBOutlet NSTextField *pause;
    IBOutlet NSTextField *session;
    IBOutlet NSDatePicker *sessionStart;
    int sessionDuration;
    int pauseDuration;
}
- (IBAction)start:(id)sender;
- (IBAction)stop:(id)sender;
@end
