//
//  main.m
//  LookUp
//
//  Created by Eugenio Tampieri on 11/11/22.
//  Copyright __MyCompanyName__ 2022. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
