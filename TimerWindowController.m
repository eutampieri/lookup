#import "TimerWindowController.h"

@implementation TimerWindowController

- (IBAction)start:(id)sender
{
    [sessionStart setDateValue: [NSDate date]];
	[session setEditable: FALSE];
	[pause setEditable: FALSE];
	pauseDuration = [[pause stringValue] intValue];
	sessionDuration = [[session stringValue] intValue];
    [NSTimer scheduledTimerWithTimeInterval:(float)sessionDuration*60
        target:self
        selector:@selector(notifySessionEnded:)
        userInfo:nil
        repeats:NO];

}

- (IBAction)stop:(id)sender
{
	[session setEditable: TRUE];
	[pause setEditable: TRUE];
	NSTimeInterval delta = [[sessionStart dateValue] timeIntervalSinceNow];
	NSAlert *alert = [NSAlert
		alertWithMessageText: @"Session ended"
		defaultButton: @"OK"
		alternateButton:nil
		otherButton: nil
		informativeTextWithFormat: [NSString stringWithFormat:@"Total duration: %f seconds", delta * -1]
	];
	[alert runModal];
}

- (void)notifySessionEnded:(NSTimer *)timer
{
    NSAlert *alert = [NSAlert
        alertWithMessageText: @"Time to take a break!"
        defaultButton: @"Take a break"
        alternateButton:nil
        otherButton: nil
        informativeTextWithFormat: [NSString stringWithFormat:@"You've been staring at the screen for %d minutes", sessionDuration]
    ];
    [alert runModal];
    [NSTimer scheduledTimerWithTimeInterval:pauseDuration
        target:self
        selector:@selector(notifyPauseEnded:)
        userInfo:nil
        repeats:NO];
}

- (void)notifyPauseEnded:(NSTimer *)timer
{
    NSAlert *alert = [NSAlert
        alertWithMessageText: @"Time to start working again!"
        defaultButton: @"Start"
        alternateButton:nil
        otherButton: nil
        informativeTextWithFormat: [NSString stringWithFormat:@"You've been inactive for %d minutes", pauseDuration]
    ];
    [alert runModal];
    [NSTimer scheduledTimerWithTimeInterval: sessionDuration * 60
        target:self
        selector:@selector(notifySessionEnded:)
        userInfo:nil
        repeats:NO];
}

@end
